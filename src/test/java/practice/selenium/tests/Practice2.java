package practice.selenium.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Practice2 {

	public static void main(String[] args) {
		String exePath = "chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exePath);
		WebDriver driver = new ChromeDriver();

		String appUrl = "http://www.DemoQA.com";
		driver.get(appUrl);

		driver.findElement(By.xpath(".//*[@id='menu-item-374']/a")).click();

		driver.navigate().back();

		driver.navigate().forward();
		
		driver.navigate().to(appUrl);

		driver.navigate().refresh();

		driver.close();

	}

}
