package practice.selenium.tests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Practice3 {

	public static void main(String[] args) {
		String exePath = "chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exePath);
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		//Practice day3 part 1
/*		driver.get("http://toolsqa.wpengine.com/automation-practice-form/");
		
		driver.findElement(By.name("firstname")).sendKeys("Carla Johnica");
		
		driver.findElement(By.name("lastname")).sendKeys("Quilop");
		
		driver.findElement(By.id("submit")).click();;
*/
		
		//Practice day3 part 2
/*		driver.get("http://toolsqa.wpengine.com/automation-practice-form/");
		
		driver.findElement(By.partialLinkText("Partial")).click();
		
		String BtagName = driver.findElement(By.id("submit")).getTagName();
		
		System.out.println(BtagName);
		
		driver.findElement(By.linkText("Link Test")).click();
*/
		
		//Practice day3 part 3
		driver.get("http://toolsqa.wpengine.com/automation-practice-form/");
		
		List<WebElement> rdBtn_Sex = driver.findElements(By.name("sex"));
		boolean bValue = false;
		bValue = rdBtn_Sex.get(0).isSelected();
		if(bValue == true){
			rdBtn_Sex.get(0).click();
		}else{
			rdBtn_Sex.get(1).click();
		}
		
		driver.findElement(By.id("exp-2")).click();
		
		List<WebElement> checkboxProfession = driver.findElements(By.name("profession"));
		int iSize = checkboxProfession.size();
		for(int i=0; i < iSize ; i++ ){
			String sValue = checkboxProfession.get(i).getAttribute("value");
			if(sValue.equals("Automation Tester")) {
				checkboxProfession.get(i).click();
			}
		}
	}
}

