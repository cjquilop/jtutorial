package practice.selenium.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Practice1 {

	public static void main(String[] args) {
		String exePath = "chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exePath);
		WebDriver driver = new ChromeDriver();
//		driver.get("http://toolsqa.wpengine.com/automation-practice-form/");
//		driver.close();
		
		String Url = "http://store.demoqa.com/";
		driver.get(Url);
				
		String Title = driver.getTitle();
		int Length = driver.getTitle().length();
		System.out.println("System Title	:	" + Title);
		System.out.println("System Lenth	=	" + Length);
		System.out.println("*********************************************************");
		
		String CurrentUrl = driver.getCurrentUrl();
		System.out.println("Page Verification");
		if(CurrentUrl.equals(Url)) {
			System.out.println("Correct page opened!");
		}
		else {
			System.out.println("Incorrect page opened!");
		}
		
		System.out.println("*********************************************************");
		driver.getPageSource();
		int PageLength = driver.getPageSource().length();
		System.out.println("Page Source Length	=	" + PageLength);
		
		driver.get("http://demoqa.com/frames-and-windows/");
		driver.findElement(By.xpath(".//*[@id='tabs-1']/div/p/a")).click();
		driver.quit();
	}
}
