package practice.selenium.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Practice7 {

	public static void main(String[] args) {
		String exePath = "chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exePath);
		WebDriver driver = new ChromeDriver();
		
		String firstName = "Carla Johnica";
		String lastName = "Quilop";
		String maritalStatus = "single";
		

		driver.get("http://demoqa.com");
		driver.manage().window().maximize();
		
		driver.findElement(By.linkText("Registration")).click();
		driver.findElement(By.id("name_3_firstname")).sendKeys(firstName);
		driver.findElement(By.id("name_3_lastname")).sendKeys(lastName);
		
		driver.findElement(By.name(maritalStatus)).click();
		
		
		
	}

}
