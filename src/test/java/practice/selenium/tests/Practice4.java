package practice.selenium.tests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Practice4 {

	public static void main(String[] args) {
		String exePath = "chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exePath);
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

/*		driver.get("http://toolsqa.wpengine.com/automation-practice-form/");
		
		Select ContinentSelect = new Select(driver.findElement(By.id("continents")));
		ContinentSelect.selectByIndex(1);
		ContinentSelect.selectByVisibleText("Africa");
		
		List<WebElement> ContinentList = ContinentSelect.getOptions();
		int sizeList = ContinentList.size();
		for(int i=0; i<sizeList; i++) {
			String ContinentText = ContinentSelect.getOptions().get(i).getText();
			System.out.println(ContinentText);
			if(ContinentText.equals("Africa")){
				ContinentSelect.selectByIndex(i);
				break;
				}
		}*/
		
		driver.get("http://toolsqa.wpengine.com/automation-practice-form/");
		Select BrowserSelect = new Select(driver.findElement(By.name("selenium_commands")));
		BrowserSelect.selectByIndex(0);
		BrowserSelect.deselectByIndex(0);
		BrowserSelect.selectByVisibleText("Navigation Commands");
		BrowserSelect.deselectByVisibleText("Navigation Commands");
		
		List<WebElement> BrowserList = BrowserSelect.getOptions();
		int BrowserSize = BrowserList.size();
		for (int i = 0; i < BrowserSize; i++) {
			WebElement e = BrowserSelect.getOptions().get(i);
			if(!e.isSelected()) {
				e.click();				
			System.out.println(e.getText());
			
		}
		BrowserSelect.deselectAll();
		driver.quit();
//	
//		for(WebElement e: BrowserList) {
//			if(!e.isSelected()) {
//				e.click();
//			}
		}
	}
}
